import json
from datetime import datetime
from dateutil import relativedelta
import pandas as pd
import requests 
import openpyxl

def convert_to_datetime(date_str):
  return datetime.fromisoformat(date_str[:-6])

# Octopus Heat Tarife nach persönlichem Angebot
def octopus_cost(hour):
  if 2 <= hour <= 6:
    return 0.2315
  elif 12 <= hour <= 16:
    return 0.2315
  elif 18 <= hour <= 21:
    return 0.3514
  else:
    return 0.3014

from config import API_KEY

# Startdatum für den Vergleich
then = datetime.strptime("2024-11-07", "%Y-%m-%d")
now = datetime.now()
hours = int((now - then).total_seconds() / 3600)

url = "https://api.tibber.com/v1-beta/gql"

# GraphQL query für die Verbräuche und Kosten pro Stunde
headers = {"Content-Type" : "application/json", "Authorization" : "Bearer " + API_KEY}
query = """
{
  viewer {
    homes {
      consumption(resolution: HOURLY, last: """ + str(hours) + """) {
        nodes {
          from
          to
          cost
          unitPrice
          unitPriceVAT
          consumption
          consumptionUnit
        }
      }
    }
  }
}
"""
response = requests.post(url=url, json={"query": query}, headers=headers) 
if response.status_code == 200: 
    data = json.loads(response.content)
    nodes = data['data']['viewer']['homes'][0]['consumption']['nodes']
    df = pd.DataFrame(pd.json_normalize(nodes))

    df['monat'] = pd.to_datetime(df['from'], format='ISO8601')
    df['tag'] = df['monat'].dt.day
    df['stunde'] = df['monat'].dt.hour
    df['brutto'] = df['unitPrice'] + df['unitPriceVAT']
    df['Tibber'] = df['consumption'] + df['brutto']
    df['heat'] = df['stunde'].apply(octopus_cost)
    df['Octopus'] = df['consumption'] + df['heat']
    df = df[['tag', 'stunde', 'consumption', 'brutto', 'heat', 'Tibber', 'Octopus']]

    print(df)

    df.to_excel("vergleich.xlsx")


#    df_consumption = df.groupby(df['monat'].dt.strftime('%Y-%m')).agg(Verbrauch=('consumption', 'sum'), Kosten=('cost', 'sum'), Strompreis=('brutto', 'mean')).reset_index().round(2)

#    print(df_consumption['Strompreis'].mean())   
#    print(df_consumption['Verbrauch'].sum())