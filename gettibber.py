import json
from datetime import datetime
import pandas as pd
import requests 

from config import API_KEY

def convert_to_datetime(date_str):
  return datetime.fromisoformat(date_str[:-6])

vertrag = "2024-02-13"
hours = ((datetime.today() - datetime.strptime(vertrag, "%Y-%m-%d")).days +1 ) * 24

url = "https://api.tibber.com/v1-beta/gql"

headers = {"Content-Type" : "application/json", "Authorization" : "Bearer " + API_KEY}
query = """
{
  viewer {
    homes {
      consumption(resolution: HOURLY, last: """ + str(hours) + """) {
        nodes {
          from
          to
          cost
          unitPrice
          unitPriceVAT
          consumption
          consumptionUnit
        }
      }
    }
  }
}
"""
response = requests.post(url=url, json={"query": query}, headers=headers) 
if response.status_code == 200: 
    data = json.loads(response.content)
    nodes = data['data']['viewer']['homes'][0]['consumption']['nodes']
    df = pd.DataFrame(pd.json_normalize(nodes))

    df['Monat'] = df['from'].apply(convert_to_datetime)
    df_consumption = df.groupby(df['Monat'].dt.strftime('%Y-%m')).agg(Verbrauch=('consumption', 'sum'), Kosten=('cost', 'sum'), Strompreis=('unitPrice', 'mean')).reset_index().round(2)

    print(df_consumption)